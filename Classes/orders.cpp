#include "orders.h"

Orders* Orders::self = NULL;
Orders::Orders()
{
}


QString Orders::getEmployeNameAndGiveJob(int definitionCode, int clientId) {
    int primaryCode;
    int secondaryCode = -1;
    int tertiaryCode = -1;
    switch (definitionCode) {
    case 0:
        primaryCode = 0;
        secondaryCode = 2;
        break;
    case 1:
        primaryCode = 1;
        secondaryCode = 2;
        break;
    case 2:
        primaryCode = 0;
        secondaryCode = 1;
        tertiaryCode = 2;
        break;
    case 3:
        primaryCode = 3;
        secondaryCode = 5;
        break;
    case 4:
        primaryCode = 4;
        secondaryCode = 5;
        break;
    case 5:
        primaryCode = 6;
        break;
    }

    QString queryString = "SELECT id, name, countRep  FROM employes "
            "LEFT OUTER JOIN ( "
            "SELECT count(clientId) as countRep, employeId FROM orders "
            "GROUP BY employeId "
            ") "
            "ON employeId = employes.id "
            "WHERE (specializationCode = '%1' or specializationCode = '%2' or specializationCode = '%3') and currentOrder = '-1' "
            "ORDER BY countRep";
    queryString = queryString.arg(primaryCode).arg(secondaryCode).arg(tertiaryCode);

    QSqlQuery query;
    query.exec(queryString);

    QString resultString;
    if (query.next()) {
        resultString = "Направить сотрудника: %1 на ремонт машины клиента: %2";
        int employeId = query.value(0).toInt();
        QString emplName = query.value(1).toString();
        queryString = "SELECT name FROM clients WHERE id = '%1'";
        queryString = queryString.arg(clientId);
        query.exec(queryString);
        query.next();
        QString clientName = query.value(0).toString();
        resultString = resultString.arg(emplName).arg(clientName);

        queryString = "INSERT INTO orders (clientId, employeId, status) VALUES (%1, %2, 1)";
        queryString = queryString.arg(clientId).arg(employeId);
        query.exec(queryString);
        queryString = "SELECT id FROM orders ORDER BY id DESC LIMIT 1";
        query.exec(queryString);
        query.next();
        int orderId = query.value(0).toInt();


        queryString = "UPDATE employes SET currentOrder = %1 WHERE id = %2";
        queryString = queryString.arg(orderId).arg(employeId);
        query.exec(queryString);
    } else {
        resultString = "Свободных сотрудников нет";
    }



    return resultString;
}

QSqlQueryModel* Orders::getCurrentOrdersModel() {
    QString queryString;
    queryString = "SELECT id, employeName ||' ремонтирует автомобиль ' || clientCar || ', клиента ' || clientName as Work FROM orders "
            "INNER JOIN ( "
            "    SELECT name as clientName, car as clientCar, id as clientsId FROM clients "
            ") on clientId = clientsId "
            "INNER JOIN ( "
            "    SELECT name as employeName, id as employesId FROM employes "
            ") on employeId = employesId "
            "WHERE status = 1";

    QSqlQuery query;

    query.exec(queryString);


    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery(query);
    model->setHeaderData(1, Qt::Horizontal, "ФИО клиента");
    model->setHeaderData(2, Qt::Horizontal, "Автомобиль ");
    model->setHeaderData(3, Qt::Horizontal, "Адрес");
    model->setHeaderData(4, Qt::Horizontal, "Автомобиль");
    model->setHeaderData(5, Qt::Horizontal, "Кол-во обращений");
    return model;
}

void Orders::workDone(int orderId) {
    QString queryString;
    queryString = "SELECT clientId, employeId FROM orders WHERE id = %1";
    queryString = queryString.arg(orderId);
    QSqlQuery query;
    query.exec(queryString);
    query.next();
    int employeId = query.value(1).toInt();
    int clientId = query.value(0).toInt();

    queryString = "SELECT numbersOfRepairs FROM clients WHERE id = %1";
    queryString = queryString.arg(clientId);
    query.exec(queryString);
    query.next();
    int clientNumbersOfRepairs = query.value(0).toInt() + 1;

    queryString = "SELECT numbersOfRepairs FROM employes WHERE id = %1";
    queryString = queryString.arg(employeId);
    query.exec(queryString);
    query.next();
    int employeNumbersOfRepairs = query.value(0).toInt() + 1;

    queryString = "UPDATE clients SET numbersOfRepairs = %1 WHERE id = %2";
    queryString = queryString.arg(clientNumbersOfRepairs).arg(clientId);
    query.exec(queryString);

    queryString = "UPDATE employes SET numbersOfRepairs = %1, currentOrder = -1 WHERE id = %2";
    queryString = queryString.arg(employeNumbersOfRepairs).arg(employeId);
    query.exec(queryString);

    queryString = "UPDATE orders SET status = 0 WHERE id = %1";
    queryString = queryString.arg(orderId);
    query.exec(queryString);

}
