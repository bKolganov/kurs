#ifndef SINGLEDB_H
#define SINGLEDB_H
#include <QtSql>

class singleDB
{
protected:
    static singleDB* self;
    QSqlDatabase DB;
    singleDB (){
        DB = QSqlDatabase::addDatabase("QSQLITE");
        DB.setDatabaseName("/Users/Boris/c++ prj/Kurs/db_kurs.sqlite");
        if (!DB.isOpen()) {
            DB.open();
        }
    }

public:
    static singleDB* instance() {
        if (!self) {
            self = new singleDB();
        }
        return self;
    }
    static bool deleteInstanse () {
        if (self) {
            delete self;
            self = NULL;
            return true;
        }
        return false;
    }
    QSqlDatabase getDB() { return DB; }
};

#endif // SINGLEDB_H
