#ifndef ORDERS_H
#define ORDERS_H
#include <QtSql>

class Orders
{
protected:
    static Orders *self;
    Orders();
public:
    static Orders* instance() {
        if (!self) {
            self = new Orders();
        }
        return self;
    }
    static bool deleteInstanse () {
        if (self) {
            delete self;
            self = NULL;
            return true;
        }
        return false;
    }

    void workDone(int orderId);
    QSqlQueryModel* getCurrentOrdersModel();
    QString getEmployeNameAndGiveJob (int definitionCode, int clientId);
};

#endif // ORDERS_H
