#include "clients.h"

Clients* Clients::self = NULL;

void Clients::add(Client *clientForAdd) {
    QSqlQuery query;
    QString queryString = "INSERT INTO clients (name, phone, address, car, numbersOfRepairs, dateOfRegistration)"
                           "VALUES ('%1', '%2', '%3', '%4', '%5', '%6')";
    queryString = queryString.arg(clientForAdd->name).arg(clientForAdd->phone)
                             .arg(clientForAdd->address).arg(clientForAdd->car)
                             .arg(clientForAdd->numbersOfRepairs).arg(clientForAdd->dateOfRegistration.currentMSecsSinceEpoch());
    query.exec(queryString);
}

Clients::Clients() {
}

QSqlTableModel* Clients::getTableModel() {
       QSqlTableModel *model = new QSqlTableModel();
       model->setTable("clients");
       model->select();
       model->setHeaderData(1, Qt::Horizontal, "ФИО");
       model->setHeaderData(2, Qt::Horizontal, "Телефон");
       model->setHeaderData(3, Qt::Horizontal, "Адрес");
       model->setHeaderData(4, Qt::Horizontal, "Автомобиль");
       model->setHeaderData(5, Qt::Horizontal, "Кол-во обращений");
       model->setEditStrategy(QSqlTableModel::OnFieldChange);
       return model;
}

QSqlQueryModel* Clients::getQueryModel()  {
    QSqlQuery query;
    query.exec("SELECT id, name || ', автомобиль: ' || car AS nameCar FROM clients ORDER BY name");
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery(query);
    return model;
}

