#ifndef CLIENTS_H
#define CLIENTS_H
#include <QtSql>

struct Client {
    QString name,
            phone,
            address,
            car;
    int numbersOfRepairs;
    QDateTime dateOfRegistration;
    Client(QString name, QString phone, QString address, QString car) {
        this->name = name;
        this->phone = phone;
        this->address = address;
        this->car = car;
        this->numbersOfRepairs = 0;
        this->dateOfRegistration = QDateTime::currentDateTime();
    }
    Client(QString name, QString phone, QString address, QString car, int numbersOfRepairs, QDateTime dateOfRegistration) {
        this->name = name;
        this->phone = phone;
        this->address = address;
        this->car = car;
        this->numbersOfRepairs = numbersOfRepairs;
        this->dateOfRegistration = dateOfRegistration;
    }
    Client () {
    }
};

class Clients {

protected:
    static Clients *self;
    Clients();

public:
    static Clients* instance () {
        if (!self) {
            self = new Clients();
        }
        return self;
    }
    static bool deleteInstanse () {
        if (self) {
            delete self;
            self = NULL;
            return true;
        }
        return false;
    }

    void add (Client* clientForAdd);
    QSqlTableModel* getTableModel();
    QSqlQueryModel* getQueryModel();
};



#endif // CLIENTS_H
