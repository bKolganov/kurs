#-------------------------------------------------
#
# Project created by QtCreator 2014-09-26T16:30:29
#
#-------------------------------------------------

QT       += core gui
QT       += sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kurs
TEMPLATE = app


SOURCES += main.cpp\
    forms/MainForm/mainwindow.cpp \
    forms/ServiceForm/serviceform.cpp \
    forms/WorkForm/workform.cpp \
    classes/clients.cpp \
    classes/orders.cpp \
    forms/DefinitionFault/definitionfault.cpp \
    forms/AdministratorForm/administrationform.cpp \
    forms/AddClientForm/addclientform.cpp \
    classes/employes.cpp \
    forms/AddEmployeFrom/addemployeform.cpp

HEADERS  += mainwindow.h \
    forms/MainForm/mainwindow.h \
    forms/ServiceForm/serviceform.h \
    forms/WorkForm/workform.h \
    classes/clients.h \
    classes/orders.h \
    forms/DefinitionFault/definitionfault.h \
    forms/AdministratorForm/administrationform.h \
    forms/AddClientForm/addclientform.h \
    classes/employes.h \
    forms/AddEmployeFrom/addemployeform.h

FORMS    += mainwindow.ui \
    forms/MainForm/mainwindow.ui \
    forms/ServiceForm/serviceform.ui \
    forms/WorkForm/workform.ui \
    forms/DefinitionFault/definitionfault.ui \
    forms/AdministratorForm/administrationform.ui \
    forms/AddClientForm/addclientform.ui \
    forms/AddEmployeFrom/addemployeform.ui

