#ifndef ADDCLIENTFORM_H
#define ADDCLIENTFORM_H

#include <QWidget>
#include "classes/clients.h"

namespace Ui {
class AddClientForm;
}

class AddClientForm : public QWidget
{
    Q_OBJECT


public:
    explicit AddClientForm(QWidget *parent = 0);
    ~AddClientForm();

protected:
    void closeEvent(QCloseEvent *);

signals:
    void addNewClient();
private slots:
    void on_pushButton_clicked();

private:
    Clients *clientsDB;
    Ui::AddClientForm *ui;
};

#endif // ADDCLIENTFORM_H
