#include "addclientform.h"
#include "ui_addclientform.h"

AddClientForm::AddClientForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddClientForm) {

    ui->setupUi(this);
    clientsDB = Clients::instance();
    ui->lineEdit->setFocus();
}

void AddClientForm::on_pushButton_clicked() {
    Client *clientForAdd = new Client(ui->lineEdit->text(),
                             ui->lineEdit_2->text(),
                             ui->lineEdit_3->text(),
                             ui->lineEdit_4->text());

    clientsDB->add(clientForAdd);
    delete clientForAdd;

    emit addNewClient();
    close();
}

void AddClientForm::closeEvent(QCloseEvent *) {
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit_3->clear();
    ui->lineEdit_4->clear();
    ui->lineEdit->setFocus();
}

AddClientForm::~AddClientForm() {
    clientsDB->deleteInstanse();
    delete ui;
}
