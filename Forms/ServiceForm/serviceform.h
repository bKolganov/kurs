#ifndef SERVICEFORM_H
#define SERVICEFORM_H

#include <QWidget>
#include <QSet>
#include <QMessageBox>

#include "forms/DefinitionFault/definitionfault.h"
#include "forms/AddClientForm/addclientform.h"

#include "classes/orders.h"

namespace Ui {
class ServiceForm;
}

class ServiceForm : public QWidget
{
    Q_OBJECT
protected:
    void showEvent(QShowEvent *);
public:
    explicit ServiceForm(QWidget *parent = 0);
    ~ServiceForm();
public slots:
    void setDefinition(QSet<QString>* stringSet);
    void setNewClient();
private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();


private:
    Orders *orders;
    Clients *clientsDB;
    QSet<QString> setOfEngineFailure;
    QSet<QString> setOfTransmissionFailure;
    QSet<QString> setOfTransmissionFailure2;

    AddClientForm *addClientForm;
    DefinitionFault *definitionForm;
    Ui::ServiceForm *ui;
};

#endif // SERVICEFORM_H
