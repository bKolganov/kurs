#include "serviceform.h"
#include "ui_serviceform.h"

ServiceForm::ServiceForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServiceForm)
{
    ui->setupUi(this);

    orders = Orders::instance();
    clientsDB = Clients::instance();
    addClientForm = new AddClientForm();
    definitionForm = new DefinitionFault();

    QObject::connect(addClientForm, SIGNAL(addNewClient()), this, SLOT(setNewClient()));
    QObject::connect(definitionForm, SIGNAL(returnDefinition(QSet<QString>*)), this, SLOT(setDefinition(QSet<QString>*)));

    setOfEngineFailure.insert("Большой расход масла");
    setOfEngineFailure.insert("Стуки двигателя при разгоне");
    setOfEngineFailure.insert("При запуске двигателя стук в течение 5 минут");
    setOfEngineFailure.insert("После прогрева двигателя запах масла в салоне");
    setOfEngineFailure.insert("При начале движения повышенное задымление из выхлопной трубы");

    setOfTransmissionFailure.insert("Гул во время езды");
    setOfTransmissionFailure.insert("Хруст при повороте");
    setOfTransmissionFailure.insert("Удары при торможении");
    setOfTransmissionFailure.insert("Стук при езде по кочкам");
    setOfTransmissionFailure.insert("Звонкий удар после начала движения");

    setOfTransmissionFailure2.insert("При разгоне авто слышен гул в области коробки");
    setOfTransmissionFailure2.insert("При включении скоростей слышен механический скрежет");
    setOfTransmissionFailure2.insert("При переключении скоростей удары в области коробки (АКПП)");
    setOfTransmissionFailure2.insert("При нажатии педали газа обороты растут, но машина не едет");
    setOfTransmissionFailure2.insert("При пуске двигателя слышен гул, при нажатии сцепления гул пропадает");



}

ServiceForm::~ServiceForm() {
    orders->deleteInstanse();
    clientsDB->deleteInstanse();
    delete ui;
    delete addClientForm;
    delete definitionForm;
}

void ServiceForm::on_pushButton_2_clicked() {
    definitionForm->show();
}

void ServiceForm::setDefinition(QSet<QString> *stringSet) {

    int engineChance = (setOfEngineFailure - *stringSet).size();
    int transmissionChance = (setOfTransmissionFailure - *stringSet).size();
    int transmissionChance2 = (setOfTransmissionFailure2 - *stringSet).size();

    if ( engineChance < transmissionChance && engineChance < transmissionChance2 ) {
        ui->comboBox->setCurrentIndex(0);
    } else if ( transmissionChance < transmissionChance2 && transmissionChance < transmissionChance2 ) {
        ui->comboBox->setCurrentIndex(1);
    } else {
        ui->comboBox->setCurrentIndex(2);
    }

}

void ServiceForm::showEvent(QShowEvent *){
    ui->tableView->setModel(clientsDB->getQueryModel());
    ui->tableView->hideColumn(0);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->verticalHeader()->setVisible(false);
    ui->tableView->horizontalHeader()->setVisible(false);
}

void ServiceForm::on_pushButton_3_clicked() {
    addClientForm->show();
}

void ServiceForm::setNewClient() {
    ui->tableView->setModel(clientsDB->getQueryModel());
}

void ServiceForm::on_pushButton_clicked() {
    QMessageBox msgBox;
    msgBox.setText(
    orders->getEmployeNameAndGiveJob(ui->comboBox->currentIndex(),
                                    ui->tableView->currentIndex().sibling(ui->tableView->currentIndex().row(),0).data().toInt())
    );
    msgBox.exec();
}
