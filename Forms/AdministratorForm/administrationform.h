#ifndef ADMINISTRATIONFORM_H
#define ADMINISTRATIONFORM_H

#include <QWidget>
#include <QtSql>

#include <classes/clients.h>

#include "forms/AddEmployeFrom/addemployeform.h"

namespace Ui {
class AdministrationForm;
}

class AdministrationForm : public QWidget
{
    Q_OBJECT
protected:
    void showEvent(QShowEvent *);
public:
    explicit AdministrationForm(QWidget *parent = 0);
    ~AdministrationForm();
public slots:
    void setNewEmploye();
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    AddEmployeForm *addFrom;
    Employes *employesDB;
    Clients *clientsDB;
    Ui::AdministrationForm *ui;
};

#endif // ADMINISTRATIONFORM_H
