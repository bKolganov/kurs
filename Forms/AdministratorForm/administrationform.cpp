#include "administrationform.h"
#include "ui_administrationform.h"

AdministrationForm::AdministrationForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdministrationForm)
{
    ui->setupUi(this);
    addFrom = new AddEmployeForm();
    clientsDB = Clients::instance();
    employesDB = Employes::instance();

    QObject::connect(addFrom, SIGNAL(addNewEmploye()), this, SLOT(setNewEmploye()));

}

void AdministrationForm::showEvent(QShowEvent *) {
    ui->tableView_2->setModel(employesDB->getTableModel());
    ui->tableView_2->hideColumn(0);
    ui->tableView_2->hideColumn(6);
    ui->tableView->setModel(clientsDB->getTableModel());
    ui->tableView->resizeColumnsToContents();
    ui->tableView_2->resizeColumnsToContents();
    ui->tableView->hideColumn(0);
    ui->tableView->hideColumn(6);

}
AdministrationForm::~AdministrationForm() {
    clientsDB->deleteInstanse();
    employesDB->deleteInstanse();
    delete addFrom;
    delete ui;
}

void AdministrationForm::on_pushButton_clicked() {
    addFrom->show();
}

void AdministrationForm::setNewEmploye() {
    ui->tableView_2->setModel(employesDB->getTableModel());
}

void AdministrationForm::on_pushButton_2_clicked()
{
    close();
}
