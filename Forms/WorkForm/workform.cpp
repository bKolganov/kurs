#include "workform.h"
#include "ui_workform.h"

WorkForm::WorkForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WorkForm)
{
    ui->setupUi(this);
    orders = Orders::instance();
}

WorkForm::~WorkForm()
{
    orders->deleteInstanse();
    delete ui;
}



void WorkForm::on_tableView_doubleClicked(const QModelIndex &index)
{
    QMessageBox qmsgBox;
    qmsgBox.setText("Учет работы");
    qmsgBox.setInformativeText("Работа выполнена");
    qmsgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    qmsgBox.setDefaultButton(QMessageBox::No);
    qmsgBox.setButtonText(QMessageBox::Yes, "Да");
    qmsgBox.setButtonText(QMessageBox::No, "Нет");

    if (qmsgBox.exec() == QMessageBox::Yes) {
        orders->workDone(index.sibling(index.row(),0).data().toInt());
        ui->tableView->setModel(orders->getCurrentOrdersModel());
      }
}


void WorkForm::showEvent(QShowEvent *) {
    ui->tableView->setModel(orders->getCurrentOrdersModel());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->hideColumn(0);
    ui->tableView->verticalHeader()->setVisible(false);
    ui->tableView->horizontalHeader()->setVisible(false);
}
void WorkForm::on_pushButton_clicked()
{
    this->close();
}
