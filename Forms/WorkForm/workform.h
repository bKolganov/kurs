#ifndef WORKFORM_H
#define WORKFORM_H

#include <QWidget>
#include <QMessageBox>
#include "classes/orders.h"

namespace Ui {
class WorkForm;
}

class WorkForm : public QWidget
{
    Q_OBJECT

protected:
    void showEvent(QShowEvent *);
public:
    explicit WorkForm(QWidget *parent = 0);
    ~WorkForm();

private slots:

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_pushButton_clicked();

private:
    Orders *orders;
    Ui::WorkForm *ui;
};

#endif // WORKFORM_H
