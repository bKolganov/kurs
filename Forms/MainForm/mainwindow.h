#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "forms/ServiceForm/serviceform.h"
#include "forms/AdministratorForm/administrationform.h"
#include "forms/WorkForm/workform.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    void setMode(bool admin = false);
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    WorkForm *workForm;
    AdministrationForm *administrationForm;
    ServiceForm *serviceForm;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
