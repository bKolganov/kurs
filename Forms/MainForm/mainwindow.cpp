#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    workForm = new WorkForm();
    serviceForm = new ServiceForm();
    administrationForm = new AdministrationForm();
}

MainWindow::~MainWindow()
{
    delete workForm;
    delete serviceForm;
    delete administrationForm;
    delete ui;

}

void MainWindow::on_pushButton_clicked()
{
    serviceForm->show();
}

void MainWindow::on_pushButton_2_clicked()
{
    administrationForm->show();
}

void MainWindow::setMode(bool admin) {
    if (!admin) {
       ui->pushButton_2->setEnabled(false);
       ui->pushButton_3->setEnabled(false);
    }
}
void MainWindow::on_pushButton_3_clicked()
{
    workForm->show();
}
