#ifndef DEFINITIONFAULT_H
#define DEFINITIONFAULT_H

#include <QWidget>
#include <QListWidgetItem>

namespace Ui {
class DefinitionFault;
}

class DefinitionFault : public QWidget
{
    Q_OBJECT

public:
    explicit DefinitionFault(QWidget *parent = 0);
    ~DefinitionFault();

protected:
    void showEvent(QShowEvent *);

private slots:
    void on_listWidget_2_itemDoubleClicked(QListWidgetItem *item);

    void on_pushButton_clicked();

private:
    Ui::DefinitionFault *ui;

signals:
    void returnDefinition (QSet<QString> *stringSet);

};

#endif // DEFINITIONFAULT_H
