#include "definitionfault.h"
#include "ui_definitionfault.h"

DefinitionFault::DefinitionFault(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DefinitionFault)
{
    ui->setupUi(this);
}

DefinitionFault::~DefinitionFault()
{
    delete ui;
}

void DefinitionFault::on_listWidget_2_itemDoubleClicked(QListWidgetItem *item)
{
    delete item;
}

void DefinitionFault::showEvent(QShowEvent *) {
    ui->listWidget_2->clear();
}

void DefinitionFault::on_pushButton_clicked()
{
    QSet<QString> *stringSet = new QSet<QString>();
    for (int i = 0; i < ui->listWidget_2->count(); ++i) {
        stringSet->insert(ui->listWidget_2->item(i)->text());
    }
    emit returnDefinition(stringSet);
    close();
    delete stringSet;
}
