#include "forms/MainForm/mainwindow.h"
#include <QtSql>
#include <QApplication>
#include <iostream>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSqlDatabase DB;
    DB = QSqlDatabase::addDatabase("QSQLITE");
    DB.setDatabaseName( QString(QCoreApplication::applicationDirPath() +" db_kurs.sqlite"));
    if (!DB.isOpen()) {
        DB.open();
    }
    QSqlQuery firstQuery;
    firstQuery.exec("CREATE TABLE IF NOT EXISTS `clients` ("
                    "`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                    "`name`	TEXT,"
                    "`phone`	TEXT,"
                    "`address`	TEXT,"
                   " `car`	TEXT,"
                    "`numbersOfRepairs`	INTEGER,"
                    "`dateOfRegistration`	INTEGER"
                ");");
    firstQuery.exec("CREATE TABLE IF NOT EXISTS `employes` ("
                        "`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
                        "`name`	TEXT,"
                       " `phone`	TEXT,"
                        "`bDate`	TEXT,"
                        "`specializationCode`	INTEGER,"
                        "`numbersOfRepairs`	INTEGER,"
                        "`dateOfRegistration`	INTEGER,"
                        "`currentOrder`	INTEGER"
                    ");");
    firstQuery.exec("CREATE TABLE IF NOT EXISTS `orders` ("
                    "`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
                    "`clientId`	INTEGER,"
                    "`employeId`	INTEGER,"
                    "`status`	INTEGER DEFAULT '0'"
                ");");
    MainWindow w;
    if (argc == 2) {
        if (strcmp(argv[1], "--admin") == 0) {
            std::cout<<"WELLCOME TO AMDIN MODE\n";
            w.setMode(true);
        } else w.setMode();
    } else w.setMode();

    w.show();


    return a.exec();
}
